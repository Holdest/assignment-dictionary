
extern read_word
extern exit
extern print_string
extern print_string_err
extern find_word
extern print_newline
extern string_length
global _start

%define BUFFER_LENGTH 256
%define RETURN_ERROR 1

section .data

	%include "words.inc"

    reading_error: db "Reading problem", 0
	not_found: db "Key not found", 0

section .text
_start:
	mov rsi, BUFFER_LENGTH
	sub rsp, BUFFER_LENGTH
	mov rdi, rsp
	call read_word
	test rax,rax
	je .reading_error
	mov rdi,rax
	mov rsi, dictionary_start
	call find_word
	test rax,rax
	je .not_found
	add rax, 8
	mov rdi, rax
	mov r10,rax
	call string_length
	add r10, rax
	inc r10
	mov rdi, r10
	mov r9, 1
	.exit:
		call print_string
		call print_newline
		call exit
	.reading_error:
		mov rdi,reading_error
		jmp .exit_err
	.not_found:
		mov rdi,not_found
		jmp .exit_err
	.exit_err:
		call print_string_err
		mov rdi, RETURN_ERROR
		call exit
