%define dictionary_start 0

%macro colon 2
%%start:
	dq dictionary_start
	db %1, 0
%2:

%define dictionary_start %%start
%endmacro

