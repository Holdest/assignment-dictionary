
extern string_equals
global find_word

section .text
; args: rdi = key string pointer, rsi = list first elem -> returns: rax = address (found) or rax = 0 (not found)
find_word:
	mov r9, rdi
	.loop:
		mov r10,rsi
		test rsi, rsi
		je .exit_not_found

        mov rdi, r9
		add rsi,8
		call string_equals
		mov rsi, r10
		test rax, rax
		jnz .exit_found

		mov rsi,[rsi]
		jmp .loop
	.exit_found:
		mov rax, rsi
		ret
	.exit_not_found:
		mov rax,0
		ret