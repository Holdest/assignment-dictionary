asm= nasm
CFLAGS=-felf64

main: lib.o dict.o main.o
	ld -o main dict.o lib.o  main.o
	
main.o: main.asm colon.inc words.inc
	$(asm) $(CFLAGS) main.asm
	
lib.o: lib.asm
	$(asm) $(CFLAGS)  lib.asm
	
dict.o: dict.asm
	$(asm) $(CFLAGS) dict.asm

clean:
	find . -type f -name '*.o' -delete
	rm -f main
	