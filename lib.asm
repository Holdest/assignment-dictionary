global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_string_err
section .text
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    mov rdi, 0
    syscall
    ret 
stdout:
    mov rax, 1
    mov rdi, 1
    syscall
    ret
stderr:
    mov rax, 1
    mov rdi, 2
    syscall
    ret
; Принимает указатель на нуль-терминированную строку в rdi, возвращает её длину
string_length:
    xor rax, rax    
    .len_loop:
        cmp byte[rdi + rax], 0  
        je .end                 
        inc rax                 
        jmp .len_loop      
    .end:
        ret

; Принимает указатель на нуль-терминированную строку в rdi, выводит её в stdout
print_string:
    xor rax, rax   
    mov rsi, rdi                
    call string_length          
    mov rdx, rax
    call stdout
    ret
print_string_err:
    xor rax, rax   
    mov rsi, rdi                
    call string_length          
    mov rdx, rax
    call stderr
    ret
; Принимает код символа в rdi и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rsi, rsp    
    mov rdx, 1      
    call stdout
    add rsp, 8
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 10
    call print_char
    ret

; Принимает число в в rdi
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbx
    push rdx 
    push rbp
    mov rbp, rsp
    mov rax, rdi
    mov rbx, 10
    dec rsp
    mov byte [rsp], 0
    .div_lp:
        xor rdx, rdx
        div rbx
        add rdx, 48
        dec rsp
        mov [rsp], dl
        test rax, rax
        jz .write_n
        jmp .div_lp
    .write_n:
        mov rdi, rsp
        call print_string
        mov rsp, rbp
        pop rbp
        pop rdx
        pop rbx
        ret

; Принимает число в в rdi
; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jl .lessN
    call print_uint
    ret
    .lessN:
        push rdi
        mov rdi,'-'
        call print_char
        pop rdi
        neg rdi
        call print_uint
        ret

; Принимает два указателя на нуль-терминированные строки в rdi и rsi, возвращает 1 если они равны, 0 иначе
string_equals:
        xor     rax, rax
        push    r9
        push    r10
        .loop:
            mov     r9b, byte[rsi]
            mov     r10b, byte[rdi]
            inc     rsi
            inc     rdi
            cmp     r9b, r10b
            jne     .ret_zero
            cmp     r10b, 0
            jne     .loop
            inc     rax
        .ret_zero:
            pop     r10
            pop     r9
            ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push rdi
    push rdx
    push rsi
    dec rsp
    mov rax, 0
    mov rdi, 0
    mov rdx, 1
    mov rsi, rsp
    syscall
    test rax, rax
    je .return
    xor rax, rax
    mov al, [rsp]
    .return:
        inc rsp
        pop rsi
        pop rdx
        pop rdi
        ret
; Принимает в rdi: адрес начала буфера,в rsi размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push    r13
    push    r14
    xor     r14, r14
    mov     r10, rsi
    mov     r13, rdi
    .init_lp:
        call    read_char
        cmp     al, 0x20
        je      .init_lp
        cmp     al, 0x9
        je      .init_lp
        cmp     al, 0xA
        je      .init_lp
        cmp     al, 0x0
        je      .read_end
        jmp     .write_char
    .read_start:
        call    read_char
        cmp     al, 0x0
        je      .read_end
        cmp     al, 0x20
        je      .read_end
        jmp     .write_char
    .write_char:
        mov     byte [r13 + r14], al
        inc     r14
        cmp     r14, r10
        je      .of
        jmp     .read_start
    .read_end:
        mov     rax, r13
        mov     byte [r13 + r14], 0
        mov     rdx, r14
        jmp     .return
    .of:
        xor     rax, rax
        xor     rdx, rdx
    .return:
        pop     r14
        pop     r13
        ret



; Принимает указатель на строку в rdi, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    push r13
    mov r13, 10
    xor rax, rax
    xor rcx, rcx
    xor rdx, rdx
    xor rsi, rsi
    .parse_char:
        mov sil, [rdi + rcx]
        cmp sil, 0x30
        jl .return
        cmp sil, 0x39
        jg .return
        inc rcx
        sub sil, 0x30
        mul r13
        add rax, rsi
        jmp .parse_char
    .return:
        mov rdx, rcx
        pop r13
        ret






; Принимает указатель на строку в rdi, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    cmp byte[rdi], '-'
    je .neg_parse
    call parse_uint
    ret
    .neg_parse:
        inc rdi
        call parse_uint
        cmp     rdx, 0
        je      .return
        neg     rax
        inc     rdx
    .return:
        ret

; Принимает указатель на строку в rdi, указатель на буфер в rsi и длину буфера в rdx
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    push r12
    push rcx
    xor rcx, rcx
    .main_lp:
        cmp rcx, rdx  
        je .of  
        mov r12, [rdi + rcx] 
        mov [rsi, rcx], r12 
        cmp r12, 0
        je .success  
        inc rcx
        jmp .main_lp
    .of:
        xor rax, rax
        jmp .exit
    .success:
        mov rax, rcx
    .exit:
        pop rcx
        pop r12
        ret
